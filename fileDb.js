const fs = require("fs");
const nanoid = require("nanoid");

let data = null;

module.exports = {
  init: () => {
    return new Promise((resolve, reject) => {
      fs.readFile("./db.json", (err, result) => {
        if (err) {
          reject(err);
        } else {
          data = JSON.parse(result);
          resolve();
        }
      });
    });
  },
  sendAllMessages: () => {
    return data.slice(-30);
  },
  sendFilteredMessages: query => {
    return data.filter(item => {
      return item.datetime > query;
    });
  },
  addMessage: item => {
    item.datetime = new Date().toISOString();
    item.id = nanoid();
    data.push(item);
    let contents = JSON.stringify(data, null, 2);

    return new Promise((resolve, reject) => {
      fs.writeFile("./db.json", contents, err => {
        if (err) {
          reject(err);
        } else {
          resolve(item);
        }
      });
    });
  }
};
