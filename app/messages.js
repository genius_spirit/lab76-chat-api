const express = require("express");
const router = express.Router();

const createRouter = db => {
  router.post("/", (req, res) => {
    if (req.body.author === "") {
      res.status(400).send({ errorName: "Field Name can't be blank" });
    }
    if (req.body.message === "") {
      res.status(400).send({ errorMessage: "Field Message can't be blank" });
    }
    db.addMessage(req.body).then(result => {
      res.send(result);
    });
  });

  router.get("/", (req, res) => {
    if (req.query.datetime) {
      const date = new Date(req.query.datetime);
      if (isNaN(date.getDate())) {
        res.status(400).send({ error: "Wrong format of date" });
      } else {
        res.send(db.sendFilteredMessages(req.query.datetime));
      }
    } else {
      res.send(db.sendAllMessages());
    }
  });

  return router;
};

module.exports = createRouter;
